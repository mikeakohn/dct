set title "Frequency"
set xlabel "Time"
set ylabel "Volume"
set term png
set output "freq.png"
plot "freq.dat" title "Frequency"

