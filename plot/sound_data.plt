set title "Sound"
set xlabel "Time"
set xrange [0:44100]
set ylabel "Volume"
set yrange [-1.0:1.0]
set term png
set output "sound_data.png"
plot "sound_data.dat" title "Sound Data" with dots

