#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#ifndef OUTPUT_H
#define OUTPUT_H

void output(const char *filename, const float *data, int count);
void output_as_int16(const char *filename, const int16_t *data, int count);

#endif

