/**
 *  midi_guitar
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPL
 *
 * Copyright 2024 by Michael Kohn
 *
 * wav.c: Read in a .wav file as an array of floats from -1.0 to 1.0.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "wav.h"

struct fmt_chunk_t
{
  uint16_t format_type;
  uint16_t channel_numbers;
  uint32_t sample_rate;
  uint32_t bytes_per_second;
  uint16_t bytes_per_sample;
  uint16_t bits_per_sample;
} fmt_chunk;

static uint32_t read_int32(FILE *in)
{
  uint32_t t;

  t = getc(in);
  t = (getc(in) << 8) | t;
  t = (getc(in) << 16) | t;
  t = (getc(in) << 24) | t;

  return t;
}

static uint16_t read_int16(FILE *in)
{
  uint16_t t;

  t = getc(in);
  t = (getc(in) << 8) | t;

  return t;
}

static int read_chars(FILE *in, char *s, int n)
{
  int t, ch;

  for (t = 0; t < n; t++)
  {
    ch = getc(in);
    if (ch == EOF) { return -1; }
    s[t] = ch;
  }

  return 0;
}

int read_wav(
  const char *filename,
  float *data,
  int size,
  int *count,
  int offset)
{
  *count = 0;

  FILE *in = fopen(filename, "rb");
  if (in == NULL) { return -1; }

  // Parse header.
  int length;
  char fourcc[4];
  char riff_type[4];

  read_chars(in, fourcc, 4);

  if (strncmp(fourcc,"RIFF", 4) != 0)
  {
    printf("This file is not a RIFF/WAV\n");
    return -1;
  }

  length = read_int32(in);
  read_chars(in, riff_type, 4);

  printf("RIFF Header\n");
  printf("----------------------------\n");
  printf("          Length: %d\n", length);
  printf("            Type: %.4s\n", riff_type);
  printf("----------------------------\n");

  // Parse FMT.
  char chunk_type[4];

  read_chars(in, chunk_type, 4);
  length = read_int32(in);
  fmt_chunk.format_type = read_int16(in);
  fmt_chunk.channel_numbers = read_int16(in);
  fmt_chunk.sample_rate = read_int32(in);
  fmt_chunk.bytes_per_second = read_int32(in);
  fmt_chunk.bytes_per_sample = read_int16(in);
  fmt_chunk.bits_per_sample = read_int16(in);

  printf("FMT Chunk\n");
  printf("----------------------------\n");
  printf("      Chunk Type: %.4s\n", chunk_type);
  printf("          Length: %d\n", length);
  printf("     Format Type: ");

  switch (fmt_chunk.format_type)
  {
    case 0: printf("Mono\n"); break;
    case 1: printf("Stereo\n"); break;
    default: printf("Unknown\n"); break;
  }

  printf(" Channel Numbers: %d\n", fmt_chunk.channel_numbers);
  printf("     Sample Rate: %d\n", fmt_chunk.sample_rate);
  printf("Bytes Per Second: %d\n", fmt_chunk.bytes_per_second);
  printf("Bytes Per Sample: ");

  switch (fmt_chunk.bytes_per_sample)
  {
    case 1:
      printf("8 bit mono (%d)\n", fmt_chunk.bytes_per_sample);
      break;
    case 2:
      printf("8 bit stereo or 16 bit mono (%d)\n", fmt_chunk.bytes_per_sample);
      break;
    case 4:
      printf("16 bit stereo (%d)\n", fmt_chunk.bytes_per_sample);
      break;
    default:
      printf("Unknown (%d)\n",fmt_chunk.bytes_per_sample);
      break;
  }

  printf(" Bits Per Sample: %d\n", fmt_chunk.bits_per_sample);
  printf("----------------------------\n");

  // Parse data.
  int data_length;

  read_chars(in, riff_type, 4);
  data_length = read_int32(in);

  printf("Data Header\n");
  printf("----------------------------\n");
  printf("          Length: %d\n", data_length);
  printf("            Type: %.4s\n", riff_type);
  printf("----------------------------\n");

  if (strncmp(riff_type, "data", 4) != 0)
  {
    printf("Error: Missing data section.\n");
    return -1;
  }

  printf("sample_count=%d\n", data_length / fmt_chunk.bytes_per_sample);

  int16_t sample;
  int sample_count = 0;
  int ch;
  int read_count, n;

  if (fmt_chunk.bits_per_sample == 8)
  {
    read_count = data_length;
    //if (fmt_chunk.format_type == 1) { read_count = read_count / 2; }

    for (n = 0; n < read_count; n++)
    {
      ch = getc(in);
      sample = ch - 128;
      sample = (sample < -127) ? -127 : sample;

      if (fmt_chunk.format_type == 1 && (n & 1) == 1) { continue; }

      if (offset != 0)
      {
        offset -= 1;
        continue;
      }

      data[sample_count++] = (float)sample / -127.0;
      if (sample_count == size) { break; }
    }
  }
    else
  if (fmt_chunk.bits_per_sample == 16)
  {
    read_count = data_length / 2;
    //if (fmt_chunk.format_type == 1) { read_count = read_count / 2; }

    for (n = 0; n < read_count; n++)
    {
      sample = read_int16(in);
      sample = (sample < -32767) ? -32767 : sample;

      if (fmt_chunk.format_type == 1 && (n & 1) == 1) { continue; }

      if (offset != 0)
      {
        offset -= 1;
        continue;
      }

      data[sample_count++] = (float)sample / 32767.0;
      if (sample_count == size) { break; }
    }
  }
    else
  {
    printf("Unknown wav format.\n");
    return -1;
  }

  *count = sample_count;

  fclose(in);

  return 0;
}

int read_wav_as_int16(
  const char *filename,
  int16_t *data,
  int size,
  int *count,
  int offset)
{
  *count = 0;

  FILE *in = fopen(filename, "rb");
  if (in == NULL) { return -1; }

  // Parse header.
  int length;
  char fourcc[4];
  char riff_type[4];

  read_chars(in, fourcc, 4);

  if (strncmp(fourcc,"RIFF", 4) != 0)
  {
    printf("This file is not a RIFF/WAV\n");
    return -1;
  }

  length = read_int32(in);
  read_chars(in, riff_type, 4);

  printf("RIFF Header\n");
  printf("----------------------------\n");
  printf("          Length: %d\n", length);
  printf("            Type: %.4s\n", riff_type);
  printf("----------------------------\n");

  // Parse FMT.
  char chunk_type[4];

  read_chars(in, chunk_type, 4);
  length = read_int32(in);
  fmt_chunk.format_type = read_int16(in);
  fmt_chunk.channel_numbers = read_int16(in);
  fmt_chunk.sample_rate = read_int32(in);
  fmt_chunk.bytes_per_second = read_int32(in);
  fmt_chunk.bytes_per_sample = read_int16(in);
  fmt_chunk.bits_per_sample = read_int16(in);

  printf("FMT Chunk\n");
  printf("----------------------------\n");
  printf("      Chunk Type: %.4s\n", chunk_type);
  printf("          Length: %d\n", length);
  printf("     Format Type: ");

  switch (fmt_chunk.format_type)
  {
    case 0: printf("Mono\n"); break;
    case 1: printf("Stereo\n"); break;
    default: printf("Unknown\n"); break;
  }

  printf(" Channel Numbers: %d\n", fmt_chunk.channel_numbers);
  printf("     Sample Rate: %d\n", fmt_chunk.sample_rate);
  printf("Bytes Per Second: %d\n", fmt_chunk.bytes_per_second);
  printf("Bytes Per Sample: ");

  switch (fmt_chunk.bytes_per_sample)
  {
    case 1:
      printf("8 bit mono (%d)\n", fmt_chunk.bytes_per_sample);
      break;
    case 2:
      printf("8 bit stereo or 16 bit mono (%d)\n", fmt_chunk.bytes_per_sample);
      break;
    case 4:
      printf("16 bit stereo (%d)\n", fmt_chunk.bytes_per_sample);
      break;
    default:
      printf("Unknown (%d)\n",fmt_chunk.bytes_per_sample);
      break;
  }

  printf(" Bits Per Sample: %d\n", fmt_chunk.bits_per_sample);
  printf("----------------------------\n");

  // Parse data.
  int data_length;

  read_chars(in, riff_type, 4);
  data_length = read_int32(in);

  printf("Data Header\n");
  printf("----------------------------\n");
  printf("          Length: %d\n", data_length);
  printf("            Type: %.4s\n", riff_type);
  printf("----------------------------\n");

  if (strncmp(riff_type, "data", 4) != 0)
  {
    printf("Error: Missing data section.\n");
    return -1;
  }

  printf("sample_count=%d\n", data_length / fmt_chunk.bytes_per_sample);

  int16_t sample;
  int sample_count = 0;
  int ch;
  int read_count, n;

  if (fmt_chunk.bits_per_sample == 8)
  {
    read_count = data_length;
    //if (fmt_chunk.format_type == 1) { read_count = read_count / 2; }

    for (n = 0; n < read_count; n++)
    {
      ch = getc(in);
      sample = ch - 128;
      sample = sample << 8;

      if (fmt_chunk.format_type == 1 && (n & 1) == 1) { continue; }

      if (offset != 0)
      {
        offset -= 1;
        continue;
      }

      data[sample_count++] = sample;
      if (sample_count == size) { break; }
    }
  }
    else
  if (fmt_chunk.bits_per_sample == 16)
  {
    read_count = data_length / 2;
    //if (fmt_chunk.format_type == 1) { read_count = read_count / 2; }

    for (n = 0; n < read_count; n++)
    {
      sample = read_int16(in);
      sample = (sample < -32767) ? -32767 : sample;

      if (fmt_chunk.format_type == 1 && (n & 1) == 1) { continue; }

      if (offset != 0)
      {
        offset -= 1;
        continue;
      }

      data[sample_count++] = sample;
      if (sample_count == size) { break; }
    }
  }
    else
  {
    printf("Unknown wav format.\n");
    return -1;
  }

  *count = sample_count;

  fclose(in);

  return 0;
}


