#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "output.h"

void output(const char *filename, const float *data, int count)
{
  FILE *fp = fopen(filename, "wb");
  int n;

  for (n = 0; n < count; n++)
  {
    fprintf(fp, "%f\n", data[n]);
  }

  fclose(fp);
}

void output_as_int16(const char *filename, const int16_t *data, int count)
{
  FILE *fp = fopen(filename, "wb");
  int n;

  for (n = 0; n < count; n++)
  {
    fprintf(fp, "%d\n", data[n]);
  }

  fclose(fp);
}

