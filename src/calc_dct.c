#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "calc_dct.h"

static int cos_table[4096];

void init_cos_table()
{
  int n;

  int length = sizeof(cos_table) / sizeof(int);
  float peak = 8192.0;

  for (n = 0; n < length; n++)
  {
    float divisor = (float)n / (float)length;

    cos_table[n] = (int)(peak * cos(divisor * (M_PI * 2)));
  }

  for (n = 0; n < length; n++)
  {
    printf("%d\n", cos_table[n]);
  }
}

int dct_ii_float(const float *data, int sample_count, float *dct, int dct_count)
{
  int k, n;
  double sum;
  const int N = sample_count;

  for (k = 0; k < dct_count; k++)
  {
    sum = 0;

    for (n = 0; n < N; n++)
    {
      const float xn = data[n];

      sum += xn * cos((M_PI / N) * ((float)n + 0.5) * (float)k);
    }

    dct[k] = sum;
  }

  return 0;
}

int dct_ii_float_octave(
  const float *data,
  int sample_count,
  float *dct,
  int dct_count)
{
  int k, n;
  double sum;
  const int N = sample_count;

  //int rate = dct_count;

  //float w0 = sqrt(1.0 / (float)sample_count) * ((float)rate / (float)sample_count);
  //float wk = sqrt(2.0 / (float)sample_count) * ((float)rate / (float)sample_count);

  for (k = 0; k < dct_count; k++)
  {
    sum = 0;

    for (n = 0; n < N; n++)
    {
      const float xn = data[n];

      sum += xn * cos(M_PI * (2.0 * (float)n + 1.0) * (float)k / (2.0 * (float)N));
    }

    //dct[k] = (k == 0 ? w0 : wk) * sum;
    dct[k] = sum;
  }

  return 0;
}

int dct_ii_float_octave_int(
  const float *data,
  int sample_count,
  float *dct,
  int dct_count)
{
  int k, n;
  double sum;
  const int N = sample_count;

  for (k = 0; k < dct_count; k++)
  {
    sum = 0;

    for (n = 0; n < N; n++)
    {
      const float xn = data[n];

      sum += xn * cos(M_PI * (2.0 * (float)n + 1.0) * (float)k / (2.0 * (float)N));
    }

    dct[k] = sum;
  }

  return 0;
}

