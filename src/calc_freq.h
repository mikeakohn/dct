
#ifndef CALC_FREQ_H
#define CALC_FREQ_H

int get_frequency(
  const int16_t *data,
  int sample_count,
  int threshold,
  int time_ms,
  int sample_rate);

#endif

