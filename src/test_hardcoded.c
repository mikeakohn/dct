#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "calc_dct.h"
#include "notes.h"
#include "output.h"
#include "wav.h"

#define SAMPLES 44100
#define FREQUENCY 440
#define RATE 44100

int main(int argc, char *argv[])
{
  float data[SAMPLES];
  int n;

  init_cos_table();

  const int toggle = SAMPLES / (FREQUENCY * 2);
  float value = 1.0;
  //int i = 0;

  for (n = 0; n < SAMPLES; n++)
  {
    if ((n % toggle) == 0) { value = -value; }
    //if ((n % toggle) == 0) { value = i % 2 == 0 ? 0 : 32767; i++;}
    data[n] = value;
  }

  output("sound_data.dat", data, SAMPLES);

  float dct[SAMPLES];
  memset(dct, 0, sizeof(dct));

  dct_ii_float_octave(data, SAMPLES, dct, RATE);
  //dct_ii_float_octave_int(data, SAMPLES, dct, RATE);

  float max = 0;
  float threshold = 10000;

  for (n = 0; n < RATE; n++)
  {
    if (dct[n] < 0) { dct[n] = -dct[n]; }
    if (max < dct[n]) { max = dct[n]; }

    if (dct[n] > threshold)
    {
      const char *name = notes_get_name(n);

      printf("%d", n);

      if (name != NULL)
      {
        printf(" %s", name);
      }

      printf("  -- strength: %f\n", dct[n]);
    }
  }

  printf("max=%f\n", max);

  output("dct.dat", dct, RATE);

  return 0;
}

