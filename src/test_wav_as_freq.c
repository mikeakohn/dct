#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "calc_freq.h"
#include "notes.h"
#include "output.h"
#include "wav.h"

#define SAMPLES 128000
#define RATE 44100

int main(int argc, char *argv[])
{
  notes_init();

  int memory_size = SAMPLES * sizeof(int16_t);

  int16_t *data = (int16_t *)malloc(memory_size);

  memset(data, 0, memory_size);

  if (argc != 4)
  {
    printf("Usage: %s <filename> <milliseconds> <threshold>\n", argv[0]);
    exit(0);
  }

  int count = 0;
  int ms = atoi(argv[2]);
  int threshold = atoi(argv[3]);

  if (read_wav_as_int16(argv[1], data, SAMPLES, &count, 0) != 0)
  {
    printf("Aborting...\n");
    exit(1);
  }

  output_as_int16("sound_data.dat", data, count);

  FILE *fp = fopen("freq.dat", "wb");

  if (fp == NULL)
  {
    printf("Could not open file for writing.\n");
    exit(0);
  } 

  int sample_size = RATE * ms / 1000;

  printf("count=%d\n", count);
  printf("sample_size=%d\n", sample_size);

  int n, last = 0;
  int delay = 0;

  for (n = 0; n < count - sample_size; n += sample_size)
  {
    int frequency = get_frequency(data + n, sample_size, threshold, ms, RATE);

    if (delay != 0 && frequency != 0)
    {
      const char *note = notes_get_name(delay);

      printf("%d %s\n", delay, note);

      delay = 0;
    }

    if (frequency != 0 && last != 0)
    {
      delay = frequency;
    }

    last = frequency;

    fprintf(fp, "%d\n", frequency);
  }

  fclose(fp);

  return 0;
}

