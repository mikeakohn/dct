
#ifndef CALC_DCT_H
#define CALC_DCT_H

void init_cos_table();

int dct_ii_float(
  const float *data,
  int sample_count,
  float *dct,
  int dct_count);

int dct_ii_float_octave(
  const float *data,
  int sample_count,
  float *dct,
  int dct_count);

int dct_ii_float_octave_int(
  const float *data,
  int sample_count,
  float *dct,
  int dct_count);

#endif

