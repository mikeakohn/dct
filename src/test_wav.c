#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "calc_dct.h"
#include "notes.h"
#include "output.h"
#include "wav.h"

#define SAMPLES 44000
#define RATE 44100

int main(int argc, char *argv[])
{
  int memory_size = SAMPLES * sizeof(float);

  float *data = (float *)malloc(memory_size);
  float *dct = (float *)malloc(memory_size);

  memset(data, 0, memory_size);
  memset(dct, 0, memory_size);

  if (argc != 4)
  {
    printf("Usage: %s <filename> <offset> <threshold: 1000>\n", argv[0]);
    exit(0);
  }

  int count = 0;
  int offset = atoi(argv[2]);
  float threshold = atof(argv[3]);

  if (read_wav(argv[1], data, SAMPLES, &count, offset) != 0)
  {
    printf("Aborting...\n");
    exit(1);
  }

  output("sound_data.dat", data, count);

  printf("count=%d\n", count);
  printf("Calculating dct...\n");

  dct_ii_float_octave(data, count, dct, 8000);

  notes_init();

  int n;
  float max_strength = 0;

  for (n = 0; n < 8000; n++)
  {
    if (dct[n] < 0) { dct[n] = -dct[n]; }
    if (max_strength < dct[n]) { max_strength = dct[n]; }

    if (dct[n] > threshold)
    {
      const char *name = notes_get_name(n);

      printf("%d", n);

      if (name != NULL)
      {
        printf(" %s", name);
      }

      printf("  -- strength: %f\n", dct[n]);
    }
  }

  printf("max_strength=%f\n", max_strength);

  output("dct.dat", dct, RATE);

  return 0;
}

