
#ifndef NOTES_H
#define NOTES_H

void notes_init();

const char *notes_get_name(int frequency);

int notes_get_midi(int frequency);

#endif

