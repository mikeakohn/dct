/**
 *  midi_guitar
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPL
 *
 * Copyright 2024 by Michael Kohn
 *
 */

#ifndef _WAV_H
#define _WAV_H

#include <stdint.h>

int read_wav(
  const char *filename,
  float *data,
  int size,
  int *count,
  int offset);

int read_wav_as_int16(
  const char *filename,
  int16_t *data,
  int size,
  int *count,
  int offset);

#endif

