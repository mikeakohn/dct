#!/usr/bin/env python3

fp = open("notes.txt", "r")

for line in fp:
  line = line.replace(",", "").replace("//", "").strip()
  tokens = line.split()

  frequency = int(float(tokens[0]))
  midi = int(tokens[1])
  name = tokens[2]

  print("  { " + str(frequency) + ", " + str(midi) + ", \"" + name + "\" },")

fp.close()

