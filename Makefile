
CFLAGS=-Wall -O3

default:
	@+make -C build

old:
	gcc -c wav.c $(CFLAGS)
	gcc -c calc_dct.c $(CFLAGS)
	gcc -o test_hardcoded test_hardcoded.c calc_dct.o wav.o -lm $(CFLAGS)
	gcc -o test_wav test_wav.c calc_dct.o wav.o -lm $(CFLAGS)
	gcc -o test_wav_no_dct test_wav_no_dct.c calc_freq.o wav.o -lm $(CFLAGS)

gnuplot_dct:
	gnuplot plot/sound_data.plt
	gnuplot plot/dct.plt

gnuplot_freq:
	gnuplot plot/sound_data16.plt
	gnuplot plot/freq.plt

clean:
	@rm -f sound_data.dat sound_data.png
	@rm -f dct.dat dct.png
	@rm -f build/*.o test_hardcoded test_wav
	@echo "Clean!"

